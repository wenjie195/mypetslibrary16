<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Cart.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    createOrder($conn,$uid);
    header('Location: ./checkout.php');
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Cart | Mypetslibrary" />
<title>Cart | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="sticky-tab menu-distance2">
	<div class="tab sticky-tab-tab">
		<button class="tablinks active hover1 tab-tab-btn" onclick="openTab(event, 'Cart')">
            <div class="green-dot"></div>
            <img src="img/cart-1.png" class="tab-icon hover1a" alt="Cart" title="Cart">
            <img src="img/cart-2.png" class="tab-icon hover1b" alt="Cart" title="Cart">
            <p class="tab-tab-p">In Cart</p>
            
        </button>
		<button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Ship')">
        	<div class="green-dot"></div>
            <img src="img/to-ship-1.png" class="tab-icon hover1a" alt="To Ship" title="To Ship">
            <img src="img/to-ship-2.png" class="tab-icon hover1b" alt="To Ship" title="To Ship">        
            <p class="tab-tab-p">To Ship</p>
        </button>
		<button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Receive')">
        	<div class="green-dot"></div>
            <img src="img/to-receive-1.png" class="tab-icon hover1a" alt="To Receive" title="To Receive">
            <img src="img/to-receive-2.png" class="tab-icon hover1b" alt="To Receive" title="To Receive">         
            <p class="tab-tab-p">To Receive</p>
        </button>
		<button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Received')">
        	<div class="green-dot"></div>
            <img src="img/received-1.png" class="tab-icon hover1a" alt="Received" title="Received">
            <img src="img/received-2.png" class="tab-icon hover1b" alt="Received" title="Received">         
            <p class="tab-tab-p">Received</p>
        </button>        
	</div>
</div>

<div class="two-menu-space width100"></div>    

<div class="width100 same-padding min-height4 adjust-padding">

	<div  id="Cart" class="tabcontent block same-padding">

		<!-- <button class="right-delete clean transparent-button">Delete All</button>
        <div class="clear"></div> -->
        
        <form method="POST">

            <?php
                $conn = connDB();
                if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
                {
                    unset($_SESSION['shoppingCart']);
                    echo " <h3> YOUR CART IS EMPTY </h3>";
                }
                else
                {
                    echo " <h3> YOUR CART IS EMPTY </h3>";
                }

                $conn->close();
            ?>
        </form>
    </div>        
</div>
<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.green-footer{
		display:none;}
</style>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>