<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

// $uid = $_SESSION['uid'];
$newUserUid = $_SESSION['newuser_uid'];
$conn = connDB();
$timestamp = time();

if(isset($_POST["image"]))
{
	$data = $_POST["image"];

	$image_array_1 = explode(";", $data);

	$image_array_2 = explode(",", $image_array_1[1]);

	$data = base64_decode($image_array_2[1]);

	// $imageName = time() . '.png';
	$imageName = $newUserUid.time() . '.png';

	file_put_contents('uploads/'.$imageName, $data);

	if(isset($_POST["image"]))
	{
		$tableName = array();
		$tableValue =  array();
		$stringType =  "";
		//echo $imageName;

		if($imageName)
		{
		array_push($tableName,"profile_pic");
		array_push($tableValue,$imageName);
		$stringType .=  "s";
		}

		array_push($tableValue,$newUserUid);
		$stringType .=  "s";
		$uploadCompanyLogo = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
		if($uploadCompanyLogo)
		{
			unset($_SESSION['newuser_uid']);
			// echo "Success";
			?>
				<div class="profile-preview">
					<img src=uploads/<?php echo $imageName  ?> class="width100" />
				</div>
			<?php
			?>
	
			<h4><?php echo "Picture Preview"; ?></h4>
		
			<a href="allUsers.php" class="red-link">
				<button class="green-button white-text clean2 edit-1-btn margin-auto">Complete</button>
			</a>
	
			<?php
		}
		else
		{
			echo "fail";
		}

	}


}

?>