<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/Services.php';
require_once dirname(__FILE__) . '/classes/States.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$states = getStates($conn);
$petriRate = getServices($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Seller | Mypetslibrary" />
<title>Edit Seller | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Edit Sellers</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
        <form method="POST" action="utilities/editSellerFunction.php" enctype="multipart/form-data">
        <?php
            if(isset($_POST['seller_uid']))
            {
                $conn = connDB();
                $sellerDetails = getSeller($conn,"WHERE uid = ? ", array("uid") ,array($_POST['seller_uid']),"s");

                // $seller = getSeller($conn, "WHERE uid =?",array("uid"),array($_POST['seller_uid']),"s");
                // $sellerDetails = $seller[0];
                // $service = $sellerDetails->getServices();
                // $serviceExp = explode(",",$service);
            ?>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p important-text">Company Name*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" required value="<?php echo $sellerDetails[0]->getCompanyName();?>" name="update_name" id="update_name">
                </div>
                <div class="dual-input second-dual-input important-text">
                    <p class="input-top-p admin-top-p important-text">Account Status</p>
                    <select class="input-name clean admin-input" required value="<?php echo $sellerDetails[0]->getAccountStatus();?>" name="update_status" id="update_status">
                        <option>Active</option>
                        <option>Inactive</option>
                    </select>
                </div>             
                
                <div class="clear"></div>  
                <div class="dual-input">
                    <p class="input-top-p admin-top-p important-text">Contact Person Name*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" required value="<?php echo $sellerDetails[0]->getContactPerson();?>" name="update_contact_person" id="update_contact_person">
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p important-text">Contact Person Contact No.*</p>
                    <!--
                    <input class="input-name clean input-textarea admin-input" type="text" required value="<?php echo $sellerDetails[0]->getContactPersonNo();?>" name="update_contact_personNo" id="update_contact_personNo">-->
                    
                    <?php
                        $str1 = $sellerDetails[0]->getContactNo();;
                        $filteredContact = str_replace( 'https://api.whatsapp.com/send?phone=6', '', $str1);
                    ?>

                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $filteredContact;?>" name="update_contact" id="update_contact">
                </div>
               <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-p admin-top-p important-text">Company State*</p>
                    <!-- <input class="input-name clean input-textarea admin-input" type="text" required value="<?php echo $sellerDetails[0]->getState();?>" name="update_state" id="update_state">   -->

                    <select class="input-name clean admin-input" type="text" name="update_state" id="update_state" required>
                        <?php
                        if($sellerDetails[0]->getState() == '')
                        {
                        ?>
                            <option selected>Select State</option>
                            <?php
                            for ($cnt=0; $cnt <count($states) ; $cnt++)
                            {
                            ?>
                                <option value="<?php echo $states[$cnt]->getStateName(); ?>">
                                    <?php echo $states[$cnt]->getStateName(); ?>
                                </option>
                            <?php
                            }
                        }
                        else
                        {
                            for ($cnt=0; $cnt <count($states) ; $cnt++)
                            {
                                if ($sellerDetails[0]->getState() == $states[$cnt]->getStateName())
                                {
                                ?>
                                    <option selected value="<?php echo $states[$cnt]->getStateName(); ?>">
                                        <?php echo $states[$cnt]->getStateName(); ?>
                                    </option>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <option value="<?php echo $states[$cnt]->getStateName(); ?>">
                                        <?php echo $states[$cnt]->getStateName(); ?>
                                    </option>
                                <?php
                                }
                            }
                        }
                        ?>
                    </select>

                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p important-text">Credits*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="1" value="<?php echo $sellerDetails[0]->getCredit();?>" name="update_credit" id="update_credit" required>      
                </div>                              
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Company Registered No.</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $sellerDetails[0]->getRegistrationNo();?>" name="update_reg" id="update_reg">
                </div>
                <!--
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Company Contact</p>

                </div>-->
                <div class="clear"></div>
                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Company Address</p>
                    <textarea class="input-name clean input-textarea address-textarea admin-address-textarea" type="text" placeholder="Please add your address" name="update_address" id="update_address"><?php echo $sellerDetails[0]->getAddress();?></textarea>
                </div>
 
                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Years of Experience</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $sellerDetails[0]->getExperience();?>" name="update_experience" id="update_experience">
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Cert</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $sellerDetails[0]->getCert();?>" name="update_cert" id="update_cert">
                </div>

                <div class="clear"></div>
                
                <!--<div class="width100 overflow"></div>-->

                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Services</p>
                    <?php $service = $sellerDetails[0]->getServices();?>
                    <?php $serviceExp = explode(",",$service);?>

                    <?php
                    if ($petriRate)
                    {
                    for ($i=0; $i <count($petriRate) ; $i++)
                    {
                    if (in_array($petriRate[$i]->getId(),$serviceExp))
                    {
                    ?>
                        <div class="filter-option">
                        <label for="<?php echo $petriRate[$i]->getServiceType() ?>" class="filter-label filter-label2 services-label"><?php echo $petriRate[$i]->getServiceType() ?>
                        <input checked="checked" type="checkbox" name="petri[]" id="<?php echo $petriRate[$i]->getServiceType() ?>" value="<?php echo $petriRate[$i]->getId() ?>" class="filter-option" />
                        <span class="checkmark"></span>
                        </label>
                        </div>
                    <?php
                    }
                    else
                    {
                    ?>
                        <div class="filter-option">
                            <label for="<?php echo $petriRate[$i]->getServiceType() ?>" class="filter-label filter-label2 services-label"><?php echo $petriRate[$i]->getServiceType() ?>
                            <input type="checkbox" name="petri[]" id="<?php echo $petriRate[$i]->getServiceType() ?>" value="<?php echo $petriRate[$i]->getId() ?>" class="filter-option" />
                            <span class="checkmark"></span>
                            </label>
                        </div>
                    <?php
                    }

                    }
                    }
                    ?>
                </div>

                <div class="clear"></div>
                <div class="width100 overflow margin-top20">
                    <p class="input-top-p admin-top-p">Type of Breed</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $sellerDetails[0]->getBreedType();?>" name="update_breed" id="update_breed">
                </div>
                <div class="clear"></div>
                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Other Info</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $sellerDetails[0]->getOtherInfo();?>" name="update_info" id="update_info">

                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $sellerDetails[0]->getInfoTwo();?>" name="update_info_two" id="update_info_two">
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $sellerDetails[0]->getInfoThree();?>" name="update_info_three" id="update_info_three">
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $sellerDetails[0]->getInfoFour();?>" name="update_info_four" id="update_info_four">

                </div>

                <div class="clear"></div>
                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Email</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $sellerDetails[0]->getEmail();?>" name="update_email" id="update_email">
                </div>

                <div class="clear"></div>

                <input type="hidden" id="uid" name="uid" value="<?php echo $sellerDetails[0]->getUid() ?>" readonly>

            <?php
            }
            ?>


        <div class="width100 overflow text-center">
        	<button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" id ="editSubmit" name ="editSubmit">Save</button>
        </div>

        </form>
        
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>
<script>
  $("#deletePic").click(function(){
    var companyPic = $(this).val();
    // alert(companyPic);
    $.ajax({
      url: 'utilities/deleteCompanyPic.php',
      data: {pic:companyPic},
      type: 'post',
      dataType: 'json',
      success:function(response){
        var success = response[0]['picDelete'];
        alert(success);
        window.location.reload();
      },
      error:function(response){
        alert("Error Occur");
      }
    });
  });
</script>
