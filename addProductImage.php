<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/Brand.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$categoryDetails = getCategory($conn);
$brandDetails = getBrand($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Product | Mypetslibrary" />
<title>Edit Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Update Profile Picture</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
        <form method="POST" action="utilities/registerProductFunction.php" enctype="multipart/form-data">
        <?php
            if(isset($_GET['uid'])){
                $conn = connDB();
                $productDetails = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($_GET['uid']),"s");
                if($productDetails){
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++){
                        ?>
                        <div class="dual-input">
                        <p class="input-top-p admin-top-p">Category*</p>
                            <select class="input-name clean admin-input" required value="<?php echo $productDetails[$cnt]->getCategory();?>" name="update_category" id="update_category">
                                <?php
                                if($productDetails[$cnt]->getCategory() == ''){
                                ?>
                                    <option selected>Please Select a Category</option>
                                    <?php
                                    for ($cntPro=0; $cntPro <count($categoryDetails) ; $cntPro++){
                                    ?>
                                        <option value="<?php echo $categoryDetails[$cntPro]->getName(); ?>"> 
                                            <?php echo $categoryDetails[$cntPro]->getName(); ?>
                                        </option>
                                    <?php
                                    }
                                }
                                else{
                                    for ($cntPro=0; $cntPro <count($categoryDetails) ; $cntPro++){
                                        if ($productDetails[$cnt]->getCategory() == $categoryDetails[$cntPro]->getName())
                                        {
                                        ?>
                                            <option selected value="<?php echo $categoryDetails[$cntPro]->getName(); ?>"> 
                                                <?php echo $categoryDetails[$cntPro]->getName(); ?>
                                            </option>
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                            <option value="<?php echo $categoryDetails[$cntPro]->getName(); ?>"> 
                                                <?php echo $categoryDetails[$cntPro]->getName(); ?>
                                            </option>
                                        <?php
                                        }
                                    }
                                }
                                ?>
                            </select>     
                        </div>
                        <div class="dual-input second-dual-input">
                            <p class="input-top-p admin-top-p">Brand*</p>
                            <select class="input-name clean admin-input" required value="<?php echo $productDetails[$cnt]->getBrand();?>"  name="update_brand" id="update_brand">
                                <!-- <option>Pedigree</option>
                                <option>Pedigree</option> -->
                                <?php
                                if($productDetails[$cnt]->getBrand() == ''){
                                ?>
                                    <option selected>Please Select a Category</option>
                                    <?php
                                    for ($cntPro=0; $cntPro <count($brandDetails) ; $cntPro++){
                                    ?>
                                        <option value="<?php echo $brandDetails[$cntPro]->getName(); ?>"> 
                                            <?php echo $brandDetails[$cntPro]->getName(); ?>
                                        </option>
                                    <?php
                                    }
                                }
                                else{
                                    for ($cntPro=0; $cntPro <count($brandDetails) ; $cntPro++){
                                        if ($productDetails[$cnt]->getBrand() == $brandDetails[$cntPro]->getName())
                                        {
                                        ?>
                                            <option selected value="<?php echo $brandDetails[$cntPro]->getName(); ?>"> 
                                                <?php echo $brandDetails[$cntPro]->getName(); ?>
                                            </option>
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                            <option value="<?php echo $brandDetails[$cntPro]->getName(); ?>"> 
                                                <?php echo $brandDetails[$cntPro]->getName(); ?>
                                            </option>
                                        <?php
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </div>        
                        <div class="clear"></div>
                        <div class="dual-input">
                            <p class="input-top-p admin-top-p">Product Name*</p>
                            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $productDetails[$cnt]->getName();?>" required name="update_name" id="update_name">      
                        </div>
                        <div class="dual-input second-dual-input">
                            <p class="input-top-p admin-top-p">SKU*</p>
                            <input class="input-name clean input-textarea admin-input" type="text" required value="<?php echo $productDetails[$cnt]->getSKU();?>" name="update_sku" id="update_sku">     
                        </div>        
                        <div class="clear"></div>
                        <div class="dual-input">
                            <p class="input-top-p admin-top-p">Product Slug (or URL, can't repeat,  Avoid Spacing and Symbol Specially"',.) Can Use - <img src="img/attention2.png" class="attention-png opacity-hover open-url" alt="Click Me!" title="Click Me!"></p>
                            <input class="input-name clean input-textarea admin-input" type="text" required value="<?php echo $productDetails[$cnt]->getSlug();?>" name="update_slug" id="update_slug">      
                        </div>
                        <div class="dual-input second-dual-input">
                                <p class="input-top-p admin-top-p">For Animal Type*</p>
                            <select class="input-name clean admin-input" required value="<?php echo $productDetails[$cnt]->getAnimalType();?>" name="update_animal_type" id="update_animal_type">
                                <!-- <option>Puppy</option>
                                <option>Kitten</option>
                                <option>Reptile</option>
                                <option>Other</option> -->
                                <?php
                                    if($productDetails[$cnt]->getAnimalType() == '')
                                    {
                                    ?>
                                        <option value="Puppy"  name='Puppy'>Puppy</option>
                                        <option value="Kitten"  name='Kitten'>Kitten</option>
                                        <option value="Reptile"  name='Reptile'>Reptile</option>
                                        <option value="Other"  name='Other'>Other</option>
                                        <option selected value=""  name=''></option>
                                    <?php
                                    } else if($productDetails[$cnt]->getAnimalType() == 'Other')
                                    {
                                    ?>
                                        <option value="Puppy"  name='Puppy'>Puppy</option>
                                        <option value="Kitten"  name='Kitten'>Kitten</option>
                                        <option value="Reptile"  name='Reptile'>Reptile</option>
                                        <option selected value="Other"  name='Other'>Other</option>
                                    <?php
                                    } else if($productDetails[$cnt]->getAnimalType() == 'Reptile')
                                    {
                                    ?>
                                        <option value="Puppy"  name='Puppy'>Puppy</option>
                                        <option value="Kitten"  name='Kitten'>Kitten</option>
                                        <option selected value="Reptile"  name='Reptile'>Reptile</option>
                                        <option value="Other"  name='Other'>Other</option>
                                        <?php
                                    } else if($productDetails[$cnt]->getAnimalType() == 'Kitten')
                                    {
                                    ?>
                                        <option value="Puppy"  name='Puppy'>Puppy</option>
                                        <option selected value="Kitten"  name='Kitten'>Kitten</option>
                                        <option value="Reptile"  name='Reptile'>Reptile</option>
                                        <option value="Other"  name='Other'>Other</option>
                                        <?php
                                    } else if($productDetails[$cnt]->getAnimalType() == 'Puppy')
                                    {
                                    ?>
                                        <option selected value="Puppy"  name='Puppy'>Puppy</option>
                                        <option value="Kitten"  name='Kitten'>Kitten</option>
                                        <option value="Reptile"  name='Reptile'>Reptile</option>
                                        <option value="Other"  name='Other'>Other</option>
                                <?php
                                }
                                ?>
                            </select>     
                        </div> 
                        <div class="clear"></div>
                        <div class="dual-input">
                            <p class="input-top-p admin-top-p">Expiry Date*</p>
                            <input class="input-name clean input-textarea admin-input" type="date" required value="<?php echo $productDetails[$cnt]->getExpiryDate();?>" name="update_expiry_date" id="update_expiry_date">         
                        </div>
                        <div class="dual-input second-dual-input">
                            <p class="input-top-p admin-top-p">Status</p>
                            <select class="input-name clean admin-input" required value="<?php echo $productDetails[$cnt]->getStatus();?>" name="update_status" id="update_status">
                                <!-- <option>Available</option>
                                <option>Sold</option> -->
                                <?php
                                    if($productDetails[$cnt]->getStatus() == '')
                                    {
                                    ?>
                                        <option value="Available"  name='Available'>Yes</option>
                                        <option value="Sold"  name='Sold'>Sold</option>
                                        <option selected value=""  name=''></option>
                                    <?php
                                    }
                                    else if($productDetails[$cnt]->getStatus() == 'Sold')
                                    {
                                    ?>
                                        <option value="Available"  name='Available'>Available</option>
                                        <option selected value="Sold"  name='Sold'>Sold</option>
                                    <?php
                                    }
                                    else if($productDetails[$cnt]->getStatus() == 'Available')
                                    {
                                    ?>
                                        <option selected value="Available"  name='Available'>Available</option>
                                        <option value="Sold"  name='Sold'>Sold</option>
                                <?php
                                }
                                ?>
                            </select>   
                        </div>  
                        <div class="clear"></div>
                        <div class="width100 overflow">
                            <p class="input-top-p admin-top-p">Product Description* (Avoid "'')</p>
                            <input class="input-name clean input-textarea admin-input" type="text" required value="<?php echo $productDetails[$cnt]->getDescription();?>" name="update_description" id="update_description">              
                        </div>
                        <div class="clear"></div>
                        <div class="width100 overflow">
                            <p class="input-top-p admin-top-p">Google Search Keyword  (Use Coma , to Separate Each Keyword, Avoid"')</p>
                            <input class="input-name clean input-textarea admin-input" type="text" placeholder="product name, brand, for sale, Penang," value="<?php echo $productDetails[$cnt]->getKeywordOne();?>" placeholder="Video Link"  required name="register_link" id="register_link">
                        </div>                  
                        <div class="clear"></div>
                        <div class="width100 overflow">
                            <p class="input-top-p admin-top-p">Video Link (Optional)</p>
                            <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $productDetails[$cnt]->getLink();?>" placeholder="Video Link"  required name="register_link" id="register_link">           
                        </div>          
                        <div class="clear"></div>
                        <div class="width100 overflow margin-bottom10">
        	                <p class="input-top-p admin-top-p">Upload Product Photo (Maximum 4)</p>
                            <p><input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" class="margin-bottom10" multiple /></p>
                             <a href="cropPetsImageP1.php">
                                <div class="profile-pic-div">
                                    <div class="update-div-div">
                                        <img src="img/update-profile-pic.jpg" class="profile-pic update-profile-pic" alt="Update Profile Picture" title="Update Profile Picture">
                                    </div>
                                    <?php 
                                        $proPic = $productDetails[$cnt]->getImageOne();
                                        if($proPic != "")
                                        {
                                        ?>
                                            <img src="userProfilePic/<?php echo $productDetails[$cnt]->getImageOne();?>" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                                        <?php
                                        }
                                        else
                                        {
                                        ?>
                                            <img src="img/profile-pic.jpg" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                                        <?php
                                        }
                                    ?>
                                </div>
                            </a> 
                        </div>
                    <?php
                    }
                }
            }
        ?>          
        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" id ="editSubmit" name ="editSubmit">Submit</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>