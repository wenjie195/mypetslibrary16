<?php
// require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$userType = $_SESSION['usertype'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
$sellerName = $userData->getName();

// $articles = getArticles($conn, "WHERE author_uid =? AND display = 'Pending' ",array("author_uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Article | Mypetslibrary" />
<title>Edit Article | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<script src="//cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>

<body class="body">

<?php include 'header.php'; ?>

<?php 
// Program to display URL of current page. 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
$link = "https"; 
else
$link = "http"; 

// Here append the common URL characters. 
$link .= "://"; 

// Append the host(domain name, ip) to the URL. 
$link .= $_SERVER['HTTP_HOST']; 

// Append the requested resource location to the URL 
$link .= $_SERVER['REQUEST_URI']; 

if(isset($_GET['id']))
{
    $referrerUidLink = $_GET['id'];
    // echo $referrerUidLink;
}
else 
{
    $referrerUidLink = "";
    // echo $referrerUidLink;
}
?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">

  <div class="width100">
    <h1 class="green-text h1-title">Edit Article</h1>
    <div class="green-border"></div>
  </div>

  <div class="border-separation">

    <div class="clear"></div>

      <?php
      if($referrerUidLink)
      {
      $conn = connDB();
      $articlesDetails = getArticles($conn,"WHERE uid = ? ", array("uid") ,array($referrerUidLink),"s");
      ?>

          <!-- <form> -->
          <form action="utilities/adminEditArticlesFunction.php" method="POST" enctype="multipart/form-data">

          <div class="dual-input">
            <p class="input-top-p admin-top-p">Title*</p>
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Title" value="<?php echo $articlesDetails[0]->getTitle() ?>" name="update_title" id="update_title" required>       
          </div>

          <?php 
          if($userType == '0')
          {
          ?>

            <div class="dual-input second-dual-input">
              <p class="input-top-p admin-top-p">Status*</p>
              <select class="input-name clean admin-input" name="update_display" id="update_display" value="<?php echo $articlesDetails[0]->getDisplay();?>" required>
                <?php
                    if($articlesDetails[0]->getDisplay() == '')
                    {
                    ?>
                        <option value="Yes"  name='Yes'>Approved</option>
                        <option value="Delete"  name='Delete'>Delete</option>
                        <option value="Rejected"  name='Rejected'>Rejected</option>
                        <option selected value="" name=''></option>
                    <?php
                    }
                    else if($articlesDetails[0]->getDisplay() == 'Yes')
                    {
                    ?>
                        <option value="Delete"  name='Delete'>Delete</option>
                        <option value="Rejected"  name='Rejected'>Rejected</option>
                        <option selected value="Yes" name='Yes'>Approved</option>
                    <?php
                    }
                    else if($articlesDetails[0]->getDisplay() == 'Delete')
                    {
                    ?>
                        <option value="Rejected"  name='Rejected'>Rejected</option>
                        <option selected value="Delete"  name='Delete'>Delete</option>
                        <option value="Yes" name='Yes'>Approved</option>
                    <?php
                    }
                    else if($articlesDetails[0]->getDisplay() == 'Rejected')
                    {
                    ?>
                        <option selected value="Rejected"  name='Rejected'>Rejected</option>
                        <option value="Delete" name='Delete'>Delete</option>
                        <option value="Yes"  name='Yes'>Approved</option>
                <?php
                }
                ?>
              </select>   
            </div>        

          <?php
          }
          else
          {
          ?>
          <?php
          }
          ?> 

          <div class="clear"></div>

          <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Article Slug/Link* (or URL, can't repeat,  Avoid Spacing and Symbol Specially"',.) Can Use -  <img src="img/attention2.png" class="attention-png opacity-hover open-url" alt="Click Me!" title="Click Me!"></p>
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Article Slug/Link" value="<?php echo $articlesDetails[0]->getArticleLink() ?>" name="article_link" id="article_link" required>              	
          </div>

          <div class="clear"></div>

          <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Keyword (Use Coma , to Separate Each Keyword, Avoid"')  <!--<img src="img/attention2.png" class="attention-png opacity-hover open-keyword" alt="Click Me!" title="Click Me!">--></p>
            <textarea class="input-name clean input-textarea admin-input keyword-input" type="text" placeholder="cute,malaysia,pet,dog,puppy," name="article_keyword" value="article_keyword" required><?php echo $articlesDetails[0]->getKeywordTwo() ?></textarea>  	
          </div>        

          <div class="clear"></div>  

          <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Upload Cover Photo</p>
            <!-- Crop Photo Feature --> 	

            <input id="file-upload" type="file" name="edit_cover_photo" id="edit_cover_photo" accept="image/*">    

            <p class="article-paragraph">
              <img src="uploadsArticle/<?php echo $articlesDetails[0]->getTitleCover();?>" class="width100" alt="Blog Title" title="Blog Title">
            </p>

            <input class="input-name clean input-textarea admin-input" type="hidden" value="<?php echo $articlesDetails[0]->getTitleCover();?>" name="article_oldpic" id="article_oldpic" readonly>  

          </div>         

          <div class="clear"></div>

          <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Photo Source/Credit <!--<img src="img/attention2.png" class="attention-png opacity-hover open-url" alt="Click Me!" title="Click Me!">--></p>
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Photo Source:" value="<?php echo $articlesDetails[0]->getImgCoverSrc() ?>" name="article_photo_source" id="article_photo_source">              	
          </div>        

          <div class="clear"></div>

          <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Article Summary/Description (Won't Appear inside the Main Content, Avoid "') <img src="img/attention2.png" class="attention-png opacity-hover open-desc" alt="Click Me!" title="Click Me!"></p>
            <textarea class="input-name clean input-textarea admin-input keyword-input desc-textarea" type="text" placeholder="Article Summary" name="article_description" id="article_description" required><?php echo $articlesDetails[0]->getKeywordOne() ?></textarea>  	
          </div>        

          <div class="clear"></div>        

          <div class="form-group publish-border input-div width100 overflow">
            <p class="input-top-p admin-top-p">Main Content (Avoid "' and don't copy paste image inside, click the icon for upload image/video/link tutorial)<img src="img/attention2.png" class="attention-png opacity-hover open-tutorial" alt="Click Me!" title="Click Me!"></p>
            <textarea name="editor" id="editor" rows="10" cols="80"  class="input-name clean input-textarea admin-input editor-input" required><?php echo $articlesDetails[0]->getParagraphOne() ?></textarea>
          </div>    

          <div class="clear"></div>    

          <input class="input-name clean input-textarea admin-input" type="hidden" placeholder="Title" value="<?php echo $articlesDetails[0]->getUid() ?>" name="article_uid" id="article_uid" readonly>   

          <div class="width100 overflow text-center">     
            <button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
          </div>

          </form>

      <?php
      }
      ?>

  </div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
    CKEDITOR.replace('editor');
</script>

</body>
</html>