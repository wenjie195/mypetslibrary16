<?php 
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Color.php';
require_once dirname(__FILE__) . '/classes/Breed.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
?>
<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add a Kitten | Mypetslibrary" />
<title>Add a Kitten | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<link rel="stylesheet" href="css/dropzone.min.css">
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
    <h1 class="green-text h1-title">Select the Profile Image of the Pet</h1>
    <div class="green-border"></div>
  </div>
  <div class="border-separation"></div>

<?php
$kittenDetails = getKitten($conn, "WHERE uid =?",array("uid"),array($_SESSION['newPetsUid']), "s");
if ($kittenDetails) {
	$imageOne = 'uploads/'.$kittenDetails[0]->getImageOne();
	$imageTwo = 'uploads/'.$kittenDetails[0]->getImageTwo();
	$imageThree = 'uploads/'.$kittenDetails[0]->getImageThree();
	$imageFour = 'uploads/'.$kittenDetails[0]->getImageFour();
	$imageFive = 'uploads/'.$kittenDetails[0]->getImageFive();
	$imageSix = 'uploads/'.$kittenDetails[0]->getImageSix();
	?>
	
	<form action="utilities/selectDefaultKittenFunction.php" method="post">
		<table id="myTableD" class="profile-table">
        <tr>
		<?php 
		if ($kittenDetails[0]->getImageOne()) {
			?>
			<td><img class="image-select" src="<?php echo $imageOne; ?>"></td>
			<?php
		}
		if ($kittenDetails[0]->getImageTwo()) {
			?>
			<td><img class="image-select" src="<?php echo $imageTwo; ?>"></td>
			<?php
		}
		if ($kittenDetails[0]->getImageThree()) {
			?>
			<td><img class="image-select" src="<?php echo $imageThree; ?>"></td>
			<?php
		}
		if ($kittenDetails[0]->getImageFour()) {
			?>
			<td><img class="image-select" src="<?php echo $imageFour; ?>"></td>
			<?php
		}
		if ($kittenDetails[0]->getImageFive()) {
			?>
			<td><img class="image-select" src="<?php echo $imageFive; ?>"></td>
			<?php
		}
		if ($kittenDetails[0]->getImageSix()) {
			?>
			<td><img class="image-select" src="<?php echo $imageSix; ?>"></td>
			<?php
		}
		
		?>
        </tr>
        <tr>
		<?php
		if ($kittenDetails[0]->getImageOne()) {
			?>
			<td><input type="radio" name="default_image" id="1" value="<?php echo $kittenDetails[0]->getImageOne() ?>"/></td>
			<?php
		}
		if ($kittenDetails[0]->getImageTwo()) {
			?>
			<td><input type="radio" name="default_image" id="2" value="<?php echo $kittenDetails[0]->getImageTwo() ?>"/></td>
			<?php
		}
		if ($kittenDetails[0]->getImageThree()) {
			?>
			<td><input type="radio" name="default_image" id="3" value="<?php echo $kittenDetails[0]->getImageThree() ?>"/></td>
			<?php
		}
		if ($kittenDetails[0]->getImageFour()) {
			?>
			<td><input type="radio" name="default_image" id="4" value="<?php echo $kittenDetails[0]->getImageFour() ?>"/></td>
			<?php
		}
		if ($kittenDetails[0]->getImageFive()) {
			?>
			<td><input type="radio" name="default_image" id="5" value="<?php echo $kittenDetails[0]->getImageFive() ?>"/></td>
			<?php
		}
		if ($kittenDetails[0]->getImageSix()) {
			?>
			<td><input type="radio" name="default_image" id="6" value="<?php echo $kittenDetails[0]->getImageSix() ?>"/></td>
			<?php
		}
		?>
        </tr>
	</table>
    <div class="width100 overflow text-center">
		<button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit">Submit</button>
	</div>
    </form>
	<?php
}
 ?>
 
 
 </div>
 <?php include 'js.php'; ?>
</body>
</html>
