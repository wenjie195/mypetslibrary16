<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$newProductUid = $_SESSION['newProduct_uid'];
$uid = $_SESSION['uid'];

$conn = connDB();
$timestamp = time();
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add a Product | Mypetslibrary" />
<title>Add a Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<link rel="stylesheet" href="css/dropzone.min.css">
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>		
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">

	<?php
        if(isset($_SESSION['newProduct_uid']))
        {
			$conn = connDB();
            $productDetails = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($_SESSION['newProduct_uid']),"s");
            if($productDetails)
            {
				for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                {
				?>
					<div class="width100 overflow text-center">
                        <img src="<?php echo "uploads/".$productDetails[$cnt]->getDefaultImage() ?>" class="" alt="" title="" />
                    </div>
					<p class="review-product-name text-center ow-margin-top20">Default Image Uploaded Successfully</p>
				<?php
				}
			}
		}
	?>

	<div class="width100 overflow text-center">   
	<a href="addProductVariation.php" class="red-link">
		<button class="green-button white-text clean2 edit-1-btn margin-auto">Add Product Variation</button>
	</a>
    </div>
    <!--
	<div class="width100 overflow text-center">   
	<a href="allProducts.php" >
		<button class="green-button white-text clean2 edit-1-btn margin-auto">Complete</button>
	</a>
	</div>-->
	<!-- <//?php 
		echo $uid . "<br>";
		echo $newProductUid;
		unset($_SESSION['newProduct_uid']);
	?> -->
</div>
<?php include 'js.php'; ?>
</body>
</html>
