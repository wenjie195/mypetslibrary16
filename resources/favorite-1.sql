-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2020 at 09:21 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_mypetslib2`
--

-- --------------------------------------------------------

--
-- Table structure for table `favorite`
--

CREATE TABLE `favorite` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `item_uid` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `favorite`
--

INSERT INTO `favorite` (`id`, `uid`, `username`, `link`, `item_uid`, `type`, `remark`, `status`, `date_created`, `date_updated`) VALUES
(1, 'c2e4a20007e868502ea2857e094af93b', NULL, '/mypetslibrary02/puppyDogForSale.php?id=fdec7776d2e1598eb361c679ee371dee', 'fdec7776d2e1598eb361c679ee371dee', 'Puppy', NULL, 'Yes', '2020-10-15 08:35:42', '2020-10-15 08:35:42'),
(2, 'c2e4a20007e868502ea2857e094af93b', NULL, '/mypetslibrary02/puppyDogForSale.php?id=ff988fc8a70f8ab9bd93feb848eb7973', 'ff988fc8a70f8ab9bd93feb848eb7973', 'Puppy', NULL, 'Yes', '2020-10-15 08:35:46', '2020-10-15 08:35:46'),
(3, 'c2e4a20007e868502ea2857e094af93b', NULL, '/mypetslibrary02/puppyDogForSale.php?id=3a8ce037a5f1913b554a991bf07e64ae', '3a8ce037a5f1913b554a991bf07e64ae', 'Puppy', NULL, 'Yes', '2020-10-15 08:35:51', '2020-10-15 08:35:51'),
(4, 'c2e4a20007e868502ea2857e094af93b', NULL, '/mypetslibrary02/puppyDogForSale.php?id=2ebd1af60651ffdce829fa8d8f309d82', '2ebd1af60651ffdce829fa8d8f309d82', 'Puppy', NULL, 'Yes', '2020-10-15 08:35:55', '2020-10-15 08:35:55'),
(5, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary02/puppyDogForSale.php?id=90075ab9581eb002175bd9a88febe543', '90075ab9581eb002175bd9a88febe543', 'Puppy', NULL, 'Yes', '2020-10-15 08:36:26', '2020-10-15 09:25:46'),
(6, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary02/puppyDogForSale.php?id=25109f070e5016b8ab6f98cb09a97748', '25109f070e5016b8ab6f98cb09a97748', 'Puppy', NULL, 'Delete', '2020-10-15 08:36:30', '2020-10-15 09:04:03'),
(7, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary02/puppyDogForSale.php?id=675a0a889779d4fbf779f58760f362ae', '675a0a889779d4fbf779f58760f362ae', 'Puppy', NULL, 'Delete', '2020-10-15 08:36:40', '2020-11-04 08:17:09'),
(8, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary02/puppyDogForSale.php?id=25109f070e5016b8ab6f98cb09a97748', '25109f070e5016b8ab6f98cb09a97748', 'Puppy', NULL, 'Delete', '2020-10-15 09:04:12', '2020-11-04 08:20:45'),
(9, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary16/puppyDogForSale.php?id=675a0a889779d4fbf779f58760f362ae', '675a0a889779d4fbf779f58760f362ae', 'Puppy', NULL, 'Delete', '2020-11-04 08:17:07', '2020-11-04 08:17:09'),
(10, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary16/puppyDogForSale.php?id=675a0a889779d4fbf779f58760f362ae', '675a0a889779d4fbf779f58760f362ae', 'Puppy', NULL, 'Delete', '2020-11-04 08:18:28', '2020-11-04 08:18:35'),
(11, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary16/puppyDogForSale.php?id=2ebd1af60651ffdce829fa8d8f309d82', '2ebd1af60651ffdce829fa8d8f309d82', 'Puppy', NULL, 'Delete', '2020-11-04 08:19:14', '2020-11-04 08:20:32'),
(12, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary16/puppyDogForSale.php?id=25109f070e5016b8ab6f98cb09a97748', '25109f070e5016b8ab6f98cb09a97748', 'Puppy', NULL, 'Delete', '2020-11-04 08:20:42', '2020-11-04 08:20:45'),
(13, '9349aede685bae49c49b0b895e465f6d', NULL, '/mypetslibrary16/puppyDogForSale.php?id=25109f070e5016b8ab6f98cb09a97748', '25109f070e5016b8ab6f98cb09a97748', 'Puppy', NULL, 'Yes', '2020-11-04 08:20:52', '2020-11-04 08:20:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
