<?php
class Variation {
    /* Member variables */
    var $id,$productId,$variation,$variationPrice,$variationStock,$variationImage,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getVariation()
    {
        return $this->variation;
    }

    /**
     * @param mixed $variation
     */
    public function setVariation($variation)
    {
        $this->variation = $variation;
    }

    /**
     * @return mixed
     */
    public function getVariationPrice()
    {
        return $this->variationPrice;
    }

    /**
     * @param mixed $variationPrice
     */
    public function setVariationPrice($variationPrice)
    {
        $this->variationPrice = $variationPrice;
    }

    /**
     * @return mixed
     */
    public function getVariationStock()
    {
        return $this->variationStock;
    }

    /**
     * @param mixed $variationStock
     */
    public function setVariationStock ($variationStock)
    {
        $this->variationStock = $variationStock;
    }

    /**
     * @return mixed
     */
    public function getVariationImage()
    {
        return $this->variationImage;
    }

    /**
     * @param mixed $variationImage
     */
    public function setVariationImage($variationImage)
    {
        $this->variationImage = $variationImage;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getVariation($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","product_id","variation","variation_price",
    "variation_stock","variation_image","date_created","date_updated");//follow database

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"variation"); //database name
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$productId,$variation,$variationPrice,$variationStock,$variationImage,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Variation;
            $class->setId($id);
            $class->setProductId($productId);
            $class->setVariation($variation);
            $class->setVariationPrice($variationPrice);
            $class->setVariationStock($variationStock);
            $class->setVariationImage($variationImage);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function createProductList($products,$cartType = 1,$postQuantityRows = null,$isIncludeNotSelectedProductToo = true){
    /*
     * CART TYPE
     * 1 = normal product display cart
     * 2 = checkout cart
     */

    $productListHtml = "";

    if(!$products){
        return $productListHtml;
    }
    //$CumPrice = 0;
    $subtotal = 0;
    $shippingfees = 0;
    $totalA = 0;
    $total = 0;
    $index = 0;

    foreach ($products as $product){
        $quantity = 0;
        if($postQuantityRows){
            $quantity = $postQuantityRows[$index];
        }

        $totalPrice = "";


        if($quantity <= 0 && !$isIncludeNotSelectedProductToo){

            $productListHtml .= '<div style="display: none;">';

        }else{
            $totalPrice = $quantity * $product->getVariationPrice();;
            // $CumPrice += $totalPrice;
            $subtotal += $totalPrice;
            $totalA = $subtotal;
            if($totalA < 100){
                $shippingfees = 30;
                $total = $totalA + 30;
            }else{
                $shippingfees = 0;
                $total = $totalA + 0;
            }
            $_SESSION['total'] = $total;

            $productListHtml .= '<div style="display: block;">';

        }

        $conn=connDB();
        //$productArray = getProduct($conn);
        $id  = $product->getId();
        // Include the database configuration file


              // Get images from the database
              $query = $conn->query("SELECT variation_image FROM variation WHERE id = '$id'");

              if($query->num_rows > 0)
        {
                  while($row = $query->fetch_assoc())
                    {
                      $imageURL = './uploads/'.$row["variation_image"];

        $productListHtml .= '

              <!-- Product -->

                  <div class="per-product-div">
                        <div class="left-product-check">
                           
                                <div class="left-cart-img-div">
                                    <img src="'.$imageURL.'" class="width100" alt="'.$product->getVariation().'" title="'.$product->getVariation().'">
                                </div>
                                
                            <div class="left-product-details">
                                <p class="text-overflow width100 green-text cart-product-title">
                                '.$product->getVariation().'
                                </p>
                                <table class="cart-details-table">
								<tr>
									<td class="grey-td">Quantity</td>
									<td>'.$quantity.'</td>
								</tr>
								<tr>
									<td class="grey-td">Total (Exclude Shipping)</td>
									<td>RM '.$totalPrice.'</td>
								</tr>                                
                                                  
                                 </table>								
								
								                     
                            </div>
                        </div> 
                </div>

            </div>
        ';
                    }
        }
        $index++;

    }

    //$productListHtml .= '<h2 class="product-name-h2">Subtotal : ' .$CumPrice.' Points<h2>';
    $productListHtml .=
    '

                <div class="clear"></div>
                <div class="no-sticky-bottom">
                        <div class="grey-border"></div>
                        <p class="left-bottom-price">Sub-total</p>
                        <p class="right-bottom-price">RM'.$subtotal.'.00</p>
                        <input type="hidden" class="right-bottom-price" value="'.$subtotal.'" name="subtotal" id="subtotal" >
                        <div class="clear"></div>
                        <p class="left-bottom-price">Shipping</p>
                        <p class="right-bottom-price"> RM' .$shippingfees.'.00</p> 
                        <div class="clear"></div>           
                </div>
                <div class="sticky-bottom-price same-padding3 ow-price-bottom">
                        <div class="grey-border"></div>
                        <p class="left-bottom-price weight900">Total</p>
                        <p class="right-bottom-price weight900">RM' .$total.'.00</p>   
                        <input type="hidden" class="right-bottom-price weight900" value="'.$total.'" name="total" id="total">
                        <div class="clear"></div>  
                        
                        <div class="width100 text-center">                                                                         
                        <button class="green-button checkout-btn clean" id="next" name="next" type="submit">COMPLETE ORDER</button>
						<p><a href="checkout.php" class="green-a">Back</a></p>
                </div>
                </div>
                <div class="sticky-distance-bottom"></div>       
            </div>
            </br>
            </br>

    ';

    return $productListHtml;
}

function getProductPrice($conn,$productId){
    $price = 0;

    $productRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");

    if($productRows){
        $price = $productRows[0]->getVariationPrice();
    }

    return $price;
}

function getProductName($conn,$productId){
    $name = "";

    $productRows = getVariation($conn," WHERE id = ? ",array("id"),array($productId),"i");

    if($productRows){
        $name = $productRows[0]->getVariation();
    }

    return $name;
}

function addToCart(){
    $shoppingCart = array();

    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

//        if($quantity > 0){
        $thisOrder = array();
        $thisOrder['productId'] = $productId;
        $thisOrder['quantity'] = $quantity;
        array_push($shoppingCart,$thisOrder);
//        }
    }

    if(count($shoppingCart) > 0) {
        $_SESSION['shoppingCart'] = $shoppingCart;
    }

}

function clearCart(){
    unset ($_SESSION["shoppingCart"]);
}

//when checkout only create order
function createOrder($conn,$uid){
    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'] && count($_SESSION['shoppingCart']) > 0){
        $shoppingCart = $_SESSION['shoppingCart'];
        $orderId = insertDynamicData($conn,"orders",array("uid"),array($uid),"s");

        if($orderId){
            $totalPrice = 0;


            for($index = 0; $index < count($shoppingCart); $index++){
                $thisCart = $shoppingCart[$index];
                $productId = $thisCart['productId'];
                // $_SESSION['productName'] = $productId;
                $quantity = $thisCart['quantity'];
                $originalPrice = getProductPrice($conn,$productId);
                $productName = getProductName($conn,$productId);
                $totalPrice += ($originalPrice * $product->getVariationPrice());

                if(!insertDynamicData($conn,"product_orders",array("product_id","product_name","order_id","quantity","final_price","original_price","discount_given"),
                    array($productId,$productName,$orderId,$quantity,$originalPrice,$originalPrice,0),"isiiddd")){
                    promptError("error creating order for product : $productId");
                }
            }


            //todo this 2 code is AFTER payment successfully done then only execute
//            insertIntoTransactionHistory($conn,$totalPrice,0,$uid,null,null,null,2,null,$orderId,3,null,null);
//            initiateReward($conn,$orderId,$uid,$totalPrice);

        }else{
            promptError("error creating order");
        }

    }
}

function getShoppingCart($conn,$cartType = 2,$isIncludeNotSelectedProductToo = false){
    $productListHtml = "";

    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
        $products = array();
        $quantities = array();
        for($index = 0; $index < count($_SESSION['shoppingCart']); $index++){
            $thisCart = $_SESSION['shoppingCart'][$index];

            $tempProductRows = getVariation($conn," WHERE id = ? ",array("id"),array($thisCart['productId']),"i");
            if($tempProductRows){
                array_push($products,$tempProductRows[0]);
                array_push($quantities,$thisCart['quantity']);
            }
        }

        if(count($products) > 0 && count($quantities) > 0 && count($products) == count($quantities)){
            $productListHtml = createProductList($products,$cartType,$quantities,$isIncludeNotSelectedProductToo);
        }
    }

    return $productListHtml;
}