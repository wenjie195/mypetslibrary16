<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $payment_status = "ACCEPTED";
    $shipping_status = "PENDING";
    $shipping_date = rewrite($_POST["shipping_date"]);
    $order_id = rewrite($_POST["order_id"]);

    $userUid = rewrite($_POST["user_uid"]);
    $subtotalValue = rewrite($_POST["subtotal_value"]);

    $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
    $previousPoints = $userDetails[0]->getPoints();

    if($previousPoints != '')
    {
        $currentPoints = $previousPoints;
    }
    else
    {
        $currentPoints = 0 ;
    }

    $totalPoints = $currentPoints + $subtotalValue;

    //for debugging
    echo "<br>";
    echo $_POST['order_id']."<br>";
    echo $payment_status."<br>";
    echo $shipping_status."<br>";
    echo $shipping_date."<br>";
    echo $userUid."<br>";
    echo $subtotalValue."<br>";
    echo $previousPoints."<br>";
    echo $totalPoints."<br>";

    if(isset($_POST['order_id']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($payment_status)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$payment_status);
            $stringType .=  "s";
        }     
        if($shipping_status)
        {
            array_push($tableName,"shipping_status");
            array_push($tableValue,$shipping_status);
            $stringType .=  "s";
        }     
        if($shipping_date)
        {
            array_push($tableName,"shipping_date");
            array_push($tableValue,$shipping_date);
            $stringType .=  "s";
        } 

        array_push($tableValue,$order_id);
        $stringType .=  "s";
        $orderUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($orderUpdated)
        {
            // $_SESSION['messageType'] = 1;
            // header('Location: ../shippingRequest.php?type=11');

            if(isset($_POST['order_id']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($totalPoints)
                {
                    array_push($tableName,"points");
                    array_push($tableValue,$totalPoints);
                    $stringType .=  "s";
                }   
                
                if(!$totalPoints)
                {
                    array_push($tableName,"points");
                    array_push($tableValue,$totalPoints);
                    $stringType .=  "s";
                }  

                array_push($tableValue,$userUid);
                $stringType .=  "s";
                $updatePoints = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($updatePoints)
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../shippingRequest.php?type=11');
                }
                else
                {
                    echo "fail";
                }
            }
            else
            {
                echo "dunno";
            }

        }
        else
        {
            //echo "fail";
            $_SESSION['messageType'] = 1;
            header('Location: ../paymentVerification.php?type=2');
        }
    }
    else
    {
        //echo "dunno";
        $_SESSION['messageType'] = 1;
        header('Location: ../paymentVerification.php?type=3');
    }

}
else 
{
    // header('Location: ../paymentVerification.php');
    header('Location: ../index.php');
}

?>