<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Favorite.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userID = $_SESSION['uid'];

function addFavorite($conn,$uid,$link,$itemUid,$type,$status)
{
     if(insertDynamicData($conn,"favorite",array("uid","link","item_uid","type","status"),
          array($uid,$link,$itemUid,$type,$status),"sssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = $userID;

     $link = rewrite($_POST['link']);
     $type = rewrite($_POST['type']);
     $itemUid = rewrite($_POST['item_uid']);

     $status = "Yes";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $link."<br>";
     // echo $type."<br>";
     // echo $itemUid."<br>";

     if(addFavorite($conn,$uid,$link,$itemUid,$type,$status))
     {
          $url = $_SESSION['url']; 
          header("location: $url");
     }
     else 
     {
          echo "fail";   
     }
 
}
else 
{
     header('Location: ../index.php');
}

?>