<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $id = $_POST["id"];

    $name = rewrite($_POST['update_name']);
    $email = rewrite($_POST['update_email']);
    $fbId = rewrite($_POST['update_fbId']);
    $phoneNo = rewrite($_POST['update_phone']);
    $birthday = rewrite($_POST['update_birthday']);
    $gender = rewrite($_POST['update_gender']);
    $status = rewrite($_POST['update_status']);
    $receiverName = rewrite($_POST['update_receiver_name']);
    $receiverNo = rewrite($_POST['update_receiver_no']);
    $state = rewrite($_POST['update_state']);
    $area = rewrite($_POST['update_area']);
    $postal = rewrite($_POST['update_postal']);
    $address = rewrite($_POST['update_address']);

    $bank = rewrite($_POST['update_bank']);
    $bankHolder = rewrite($_POST['update_bank_holder']);
    $bankNo = rewrite($_POST['update_bank_no']);
    $nameOnCard = rewrite($_POST['update_name_onCard']);
    $cardNo = rewrite($_POST['update_card_no']);
    $cardType = rewrite($_POST['update_card_type']);
    $expiry= rewrite($_POST['update_expiry']);
    $ccv = rewrite($_POST['update_ccv']);
    $postalCode = rewrite($_POST['update_postal_code']);
    $billingAddress = rewrite($_POST['update_billing_address']);

    //$user = getUser($conn," id = ? ",array("id"),array($id),"i");

    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $uid."<br>";
    //  echo $name."<br>";
    //  echo $email."<br>";
    //  echo $fbId."<br>";
    //  echo $phoneNo."<br>";
    //  echo $birthday."<br>";
}

if(isset($_POST['editSubmit']))
{   
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    //echo "save to database";
    if($name)
    {
        array_push($tableName,"name");
        array_push($tableValue,$name);
        $stringType .=  "s";
    }

    if($email)
    {
        array_push($tableName,"email");
        array_push($tableValue,$email);
        $stringType .=  "s";
    }

    if($fbId)
    {
        array_push($tableName,"fb_id");
        array_push($tableValue,$fbId);
        $stringType .=  "s";
    }

    if($phoneNo)
    {
        array_push($tableName,"phone_no");
        array_push($tableValue,$phoneNo);
        $stringType .=  "s";
    }

    if($birthday)
    {
        array_push($tableName,"birthday");
        array_push($tableValue,$birthday);
        $stringType .=  "s";
    }

    if($gender)
    {
        array_push($tableName,"gender");
        array_push($tableValue,$gender);
        $stringType .=  "s";
    }

    if($status)
    {
        array_push($tableName,"account_status");
        array_push($tableValue,$status);
        $stringType .=  "s";
    }

    if($receiverName)
    {
        array_push($tableName,"receiver_name");
        array_push($tableValue,$receiverName);
        $stringType .=  "s";
    }

    if($receiverNo)
    {
        array_push($tableName,"receiver_contact_no");
        array_push($tableValue,$receiverNo);
        $stringType .=  "s";
    }

    if($state)
    {
        array_push($tableName,"shipping_state");
        array_push($tableValue,$state);
        $stringType .=  "s";
    }

    if($area)
    {
        array_push($tableName,"shipping_area");
        array_push($tableValue,$area);
        $stringType .=  "s";
    }

    if($postal)
    {
        array_push($tableName,"shipping_postal_code");
        array_push($tableValue,$postal);
        $stringType .=  "s";
    }

    if($address)
    {
        array_push($tableName,"shipping_address");
        array_push($tableValue,$address);
        $stringType .=  "s";
    }

    if($bank)
    {
        array_push($tableName,"bank_name");
        array_push($tableValue,$bank);
        $stringType .=  "s";
    }

    if($bankHolder)
    {
        array_push($tableName,"bank_account_holder");
        array_push($tableValue,$bankHolder);
        $stringType .=  "s";
    }

    if($bankNo)
    {
        array_push($tableName,"bank_account_no");
        array_push($tableValue,$bankNo);
        $stringType .=  "s";
    }

    if($nameOnCard)
    {
        array_push($tableName,"name_on_card");
        array_push($tableValue,$nameOnCard);
        $stringType .=  "s";
    }

    if($cardNo)
    {
        array_push($tableName,"card_no");
        array_push($tableValue,$cardNo);
        $stringType .=  "s";
    }

    if($cardType)
    {
        array_push($tableName,"card_type");
        array_push($tableValue,$cardType);
        $stringType .=  "s";
    }

    if($expiry)
    {
        array_push($tableName,"expiry_date");
        array_push($tableValue,$expiry);
        $stringType .=  "s";
    }

    if($ccv)
    {
        array_push($tableName,"ccv");
        array_push($tableValue,$ccv);
        $stringType .=  "s";
    }

    if($postalCode)
    {
        array_push($tableName,"postal_code");
        array_push($tableValue,$postalCode);
        $stringType .=  "s";
    }

    if($billingAddress)
    {
        array_push($tableName,"billing_address");
        array_push($tableValue,$billingAddress);
        $stringType .=  "s";
    }

    array_push($tableValue,$id);
    $stringType .=  "s";
    $updateUserDetails = updateDynamicData($conn,"user"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($updateUserDetails)
    {
        $_SESSION['messageType'] = 1;
        echo "<script>alert('Data Updated and Stored !');window.location='../allUsers.php'</script>"; 
    }
    else
    {
        $_SESSION['messageType'] = 1;
        echo "<script>alert('Fail to Update Data !');window.location='../allUsers.php'</script>"; 
    }

}
else
{
    header('Location: ../index.php');
    // $_SESSION['messageType'] = 1;
    // header('Location: ../editProfile.php?type=1');
}

?>