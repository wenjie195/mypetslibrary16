<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/CreditCard.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = $_SESSION['uid'];

    $cardId = rewrite($_POST["card_id"]);

    $cardName = rewrite($_POST["edit_card_name"]);
    $cardNo = rewrite($_POST["edit_card_no"]);
    $cardType = rewrite($_POST["edit_card_type"]);
    $expiryDate = rewrite($_POST["edit_expiry_date"]);
    $ccv = rewrite($_POST["edit_ccv"]);
    $postalCode = rewrite($_POST["edit_postal_code"]);
    $billingAddress = rewrite($_POST["edit_billing_address"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $cardId."<br>";
    // echo $cardName."<br>";

    $card = getCreditCard($conn," id = ?",array("id"),array($cardId),"i");    

    if(!$card)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($cardName)
        {
            array_push($tableName,"name_on_card");
            array_push($tableValue,$cardName);
            $stringType .=  "s";
        }
        if($cardNo)
        {
            array_push($tableName,"card_no");
            array_push($tableValue,$cardNo);
            $stringType .=  "i";
        }
        if($cardType)
        {
            array_push($tableName,"card_type");
            array_push($tableValue,$cardType);
            $stringType .=  "s";
        }
        if($expiryDate)
        {
            array_push($tableName,"expiry_date");
            array_push($tableValue,$expiryDate);
            $stringType .=  "s";
        }
        if($ccv)
        {
            array_push($tableName,"ccv");
            array_push($tableValue,$ccv);
            $stringType .=  "i";
        }
        if($postalCode)
        {
            array_push($tableName,"postal_code");
            array_push($tableValue,$postalCode);
            $stringType .=  "s";
        }
        if($billingAddress)
        {
            array_push($tableName,"billing_address");
            array_push($tableValue,$billingAddress);
            $stringType .=  "s";
        }

        array_push($tableValue,$cardId);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"credit_card"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../editBankDetails.php?type=3');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../editBankDetails.php?type=5');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../editBankDetails.php?type=6');
    }

}
else 
{
    header('Location: ../index.php');
}
?>