<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/product.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST['uid']);
    $variation= rewrite($_POST['update_variation']);
    $price = rewrite($_POST['update_variation_price']);
    $stock = rewrite($_POST['update_variation_stock']);

    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $variation."<br>";
    //  echo $price ."<br>";
    //  echo $stock."<br>";
}

if(isset($_POST['editSubmit']))
{   
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    //echo "save to database";
    if($variation)
    {
        array_push($tableName,"variation");
        array_push($tableValue,$variation);
        $stringType .=  "s";
    }

    if($price)
    {
        array_push($tableName,"variation_price");
        array_push($tableValue,$price);
        $stringType .=  "s";
    }

    if($stock)
    {
        array_push($tableName,"variation_stock");
        array_push($tableValue,$stock);
        $stringType .=  "s";
    }
    array_push($tableValue,$uid);
    $stringType .=  "s";
    $updateProductDetails = updateDynamicData($conn,"variation"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    if($updateProductDetails)
    {    
        $_SESSION['messageType'] = 1;
        echo "<script>alert('Data Updated and Stored !');window.location='../allProducts.php'</script>"; 
    }
    else
    {
        $_SESSION['messageType'] = 1;
        echo "<script>alert('Fail to Update Data !');window.location='../allProducts.php'</script>"; 
    }
}
else
{
    header('Location: ../index.php');
    // $_SESSION['messageType'] = 1;
    // header('Location: ../editProfile.php?type=1');
}

?>