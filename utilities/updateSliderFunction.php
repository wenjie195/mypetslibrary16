<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Seller.php';
require_once dirname(__FILE__) . '/../classes/Slider.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["slider_uid"]);

    $link = rewrite($_POST["update_link"]);

    // $imgOne = rewrite($_POST['update_image_one']);
    $imgOne = $_FILES['update_image_one']['name'];
    if($imgOne != "")
    {
        $imageOne = $timestamp.$uid.$_FILES['update_image_one']['name'];
        $target_dir = "../uploadsSlider/";
        $target_file = $target_dir . basename($_FILES["update_image_one"]["name"]);
        // Select file type
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Valid file extensions
        $extensions_arr = array("jpg","jpeg","png","gif");
        if( in_array($imageFileType,$extensions_arr) )
        {
        move_uploaded_file($_FILES['update_image_one']['tmp_name'],$target_dir.$imageOne);
        }
    }
    else
    {    
        $imageOne = rewrite($_POST["old_img"]);
    }

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    if(!$seller)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($link)
        {
            array_push($tableName,"link");
            array_push($tableValue,$link);
            $stringType .=  "s";
        }
        if($imageOne)
        {
            array_push($tableName,"img_name");
            array_push($tableValue,$imageOne);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $profileUpdated = updateDynamicData($conn,"slider"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($profileUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../slider.php?type=6');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../slider.php?type=7');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../slider.php?type=8');
    }

}
else 
{
    header('Location: ../index.php');
}
?>
