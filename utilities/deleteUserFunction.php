<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/../classes/Seller.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $userUid = rewrite($_POST["user_uid"]);
    $status = "Delete";

    // $userType = "5";
    $userType = "7";

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $sellerUid."<br>";

    if(isset($_POST['user_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"account_status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }    

        if($userType)
        {
            array_push($tableName,"user_type");
            array_push($tableValue,$userType);
            $stringType .=  "i";
        } 

        array_push($tableValue,$userUid);
        $stringType .=  "s";
        $deleteUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($deleteUser)
        {
            // echo "credit card deleted";
            $_SESSION['messageType'] = 1;
            header('Location: ../allUsers.php?type=1');
        }
        else
        {
            // echo "fail";
            $_SESSION['messageType'] = 1;
            header('Location: ../allUsers.php?type=2');
        }
    }
    else
    {
        // echo "error";
        $_SESSION['messageType'] = 1;
        header('Location: ../allUsers.php?type=3');
    }
    
}
else
{
     header('Location: ../index.php');
}
?>