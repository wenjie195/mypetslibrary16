<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Favorite.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$_SESSION['url'] = $_SERVER['REQUEST_URI'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$allFavorite = getFavorite($conn, "WHERE uid =? AND status = 'Yes' ",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Profile | Mypetslibrary" />
<title>Profile | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

	<?php
    if ($_SESSION['fb_login'])
    {
    ?>
      <div class="width100 same-padding overflow min-height menu-distance2">
     		<div class="width100 overflow profile-top-div">
            	<a href="updateProfilePic.php">
                    <div class="profile-pic-div">
                        <div class="update-div-div">
                            <!-- <img src="img/update-profile-pic.jpg" class="profile-pic update-profile-pic" alt="Update Profile Picture" title="Update Profile Picture"> -->
                        </div>
                        <?php
                            if(isset($_SESSION['user_image']))
                            {
                            ?>
                                <img src="<?php echo $_SESSION['user_image'] ?>" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                            <?php
                            }
                            else
                            {
                            ?>
                                <img src="img/profile-pic.jpg" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                            <?php
                            }
                        ?>
                    </div>
                </a>
                <div class="next-to-profile-pic-div">
                    <p class="profile-greeting">Hi, <?php echo $_SESSION['user_name'];?><br>Points: 0</p>
                    <a href="editProfile.php" class="green-a edit-a hover1"><img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit Profile</a>
                </div>
            </div>

            <div class="clear"></div>

            <a href="editAddress.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/address.png" class="left-icon-png" alt="Shipping Address" title="Shipping Address"></div>
                    <div class="right-profile-content">Shipping Address</div>
                </div>
            </a>
            <a href="bankDetailsEdit.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/banking.png" class="left-icon-png" alt="Banking Details" title="Banking Details"></div>
                    <div class="right-profile-content">Banking Details</div>
                </div>
            </a>
            <a href="logout.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/logout.png" class="left-icon-png" alt="Logout" title="Logout"></div>
                    <div class="right-profile-content">Logout</div>
                </div>
            </a>
    	</div>
    <?php
    }
    else
    {
    ?>
        <div class="width100 same-padding overflow min-height menu-distance2">
     		<div class="width100 overflow profile-top-div">
            	<a href="updateProfilePic.php">
                    <div class="profile-pic-div">
                        <div class="update-div-div">
                            <img src="img/update-profile-pic.jpg" class="profile-pic update-profile-pic" alt="Update Profile Picture" title="Update Profile Picture">
                        </div>
                        <!-- <img src="img/profile-pic.jpg" class="profile-pic" alt="Profile Picture" title="Profile Picture"> -->
                        <?php
                            $proPic = $userData->getProfilePic();
                            if($proPic != "")
                            {
                            ?>
                                <img src="userProfilePic/<?php echo $userData->getProfilePic();?>" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                            <?php
                            }
                            else
                            {
                            ?>
                                <img src="img/profile-pic.jpg" class="profile-pic" alt="Profile Picture" title="Profile Picture">
                            <?php
                            }
                        ?>
                    </div>
                </a>
                <div class="next-to-profile-pic-div">
                	<!-- <p class="profile-greeting">Hi, Username</p> -->
                    <p class="profile-greeting">Hi, <?php echo $userData->getName();?><br>Points: <?php echo $userData->getPoints();?></p>
                    <a href="editProfile.php" class="green-a edit-a hover1"><img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit Profile</a>
                </div>
            </div>

            <div class="clear"></div>

            <a href="editAddress.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/address.png" class="left-icon-png" alt="Shipping Address" title="Shipping Address"></div>
                    <div class="right-profile-content">Shipping Address</div>
                </div>
            </a>
            <a href="bankDetailsEdit.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/banking.png" class="left-icon-png" alt="Banking Details" title="Banking Details"></div>
                    <div class="right-profile-content">Banking Details</div>
                </div>
            </a>
            <a href="logout.php">
                <div class="profile-line-divider opacity-hover">
                    <div class="left-icon-div"><img src="img/logout.png" class="left-icon-png" alt="Logout" title="Logout"></div>
                    <div class="right-profile-content">Logout</div>
                </div>
            </a>

            <div class="clear"></div>


            <div class="width103">

            <?php
                if($allFavorite)
                {
                    for($cnt = 0;$cnt < count($allFavorite) ;$cnt++)
                    {
                    ?>
                        <?php $petsUid = $allFavorite[$cnt]->getItemUid();?>

                        <?php
                        $conn = connDB();
                        $favoritePets = getPetsDetails($conn,"WHERE uid = ? ", array("uid") ,array($petsUid),"s");
                        if($favoritePets)
                        {
                            for($cntAA = 0;$cntAA < count($favoritePets) ;$cntAA++)
                            {
                            ?>

                                <!-- <a href='petsForSale.php?id=<?php echo $petsUid;?>'> -->
                                <a href='puppyDogForSale.php?id=<?php echo $petsUid;?>'>
                                    <div class="shadow-white-box four-box-size opacity-hover">
                                    <div class="square">     
                                        <div class="width100 white-bg content">
                                            <img src="uploads/<?php echo $favoritePets[$cntAA]->getDefaultImage();?>" alt="" title="" class="width100 two-border-radius">
                                        </div>
                                    </div> 

                                    <?php 
                                        $status = $favoritePets[$cntAA]->getStatus();
                                        if($status == 'Sold')
                                        {
                                        ?>
                                            <div class="sold-label sold-label3">Sold</div>
                                        <?php
                                        }
                                        else
                                        {}
                                    ?>

                                    <div class="width100 product-details-div">

                                    <?php 
                                        $petGender = $favoritePets[$cntAA]->getGender();
                                        if($petGender == 'Female')
                                        {
                                            $petsGender = 'F';
                                        }
                                        elseif($petGender == 'Male')
                                        {
                                            $petsGender = 'M';
                                        }
                                    ?>

                                    <p class="width100 text-overflow slider-product-name"><?php echo $favoritePets[$cntAA]->getBreed();?> | <?php echo $petsGender ;?></p>
                                    <p class="slider-product-name">
                                        RM<?php $length = strlen($favoritePets[$cntAA]->getPrice());?>
                                        <?php 
                                        if($length == 2)
                                        {
                                        $hiddenPrice = "X";
                                        }
                                        elseif($length == 3)
                                        {
                                        $hiddenPrice = "XX";
                                        }
                                        elseif($length == 4)
                                        {
                                        $hiddenPrice = "XXX";
                                        }
                                        elseif($length == 5)
                                        {
                                        $hiddenPrice = "XXXX";
                                        }
                                        elseif($length == 6)
                                        {
                                        $hiddenPrice = "XXXXX";
                                        }
                                        elseif($length == 7)
                                        {
                                        $hiddenPrice = "XXXXXX";
                                        }
                                        ?>
                                        <?php echo substr($favoritePets[$cntAA]->getPrice(),0,1); echo $hiddenPrice;?>
                                    </p>
                                    </div>

                                    </div>
                                </a> 

                            <?php
                            }
                            ?>
                        <?php
                        }
                        ?>
                        



                    <?php
                    }
                }
            ?>  
            </div>


            <div class="clear"></div>

            <div class="width100 scroll-div border-separation">
                <p class="profile-greeting">Favorite Pets</p>
                <table class="green-table width100">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Link</th>
                            <th>Pet Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if($allFavorite)
                            {
                                for($cnt = 0;$cnt < count($allFavorite) ;$cnt++)
                                {
                                ?>
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td><a href="<?php echo $allFavorite[$cnt]->getLink();?>" target="_blank"><?php echo $allFavorite[$cnt]->getLink();?></a></td>
                                        <td><?php echo $allFavorite[$cnt]->getType();?></td>

                                        <td>
                                            <form action="utilities/removeFavoriteTwoFunction.php" method="POST"> 
                                                <button class="clean hover1 img-btn" type="submit" name="item_link" value="<?php echo $allFavorite[$cnt]->getLink();?>">
                                                    <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                                    <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                <?php
                                }
                            }
                        ?>                                 
                    </tbody>
                </table>
            </div>

    	</div>
    <?php
    }
    ?>

<div class="clear"></div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Profile Successfully !";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Update Password Successfully !";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Update Shipping Address Successfully !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Profile Picture Updated !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
