<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <img src="img/mypetslibrary.png" class="logo-img" alt="MyPetsLibrary" title="MyPetsLibrary">
            </div>
            <div class="right-menu-div float-right">
          
                    <a href="sellerDashboard.php"   class="menu-padding hover1">
                        <img src="img/home.png" class="menu-img hover1a" alt="Dashboard" title="Dashboard">
                        <img src="img/home2.png" class="menu-img hover1b" alt="Dashboard" title="Dashboard">
                    </a>


                <div class="dropdown hover1">
                	<a  class="menu-margin menu-padding hover1">
                            	<img src="img/seller.png" class="menu-img hover1a" alt="Seller" title="Seller">
                                <img src="img/seller2.png" class="menu-img hover1b" alt="Seller" title="Seller">
                	</a>
                	<div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="profilePreview.php"  class="menu-padding dropdown-a">Profile</a></p>
                                <p class="dropdown-p"><a href="editSellerProfile.php"  class="menu-padding dropdown-a">Edit Profile</a></p>
                	</div>
                </div>  
                <div class="dropdown hover1">
                	<a  class="menu-margin menu-padding hover1">
                            	<img src="img/pet.png" class="menu-img hover1a" alt="Pet" title="Pet">
                                <img src="img/pet2.png" class="menu-img hover1b" alt="Pet" title="Pet">
                	</a>
                	<div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="sellerPetSummary.php"  class="menu-padding dropdown-a">Pet Summary</a></p>
                                <p class="dropdown-p"><a href="sellerAllPets.php"  class="menu-padding dropdown-a">All Pets</a></p>
                                <p class="dropdown-p"><a href="sellerAllPuppies.php"  class="menu-padding dropdown-a">All Puppies</a></p>
                                <p class="dropdown-p"><a href="sellerAllKittens.php"  class="menu-padding dropdown-a">All Kittens</a></p>
                                <p class="dropdown-p"><a href="sellerAllReptiles.php"  class="menu-padding dropdown-a">All Reptiles</a></p>
                                <p class="dropdown-p"><a href="sellerPendingPets.php"  class="menu-padding dropdown-a">Pending Pets</a></p>
                	</div>
                </div>                 
                             
                <div class="dropdown hover1">
                	<a  class="menu-margin menu-padding hover1">
                            	<img src="img/article.png" class="menu-img hover1a" alt="Article" title="Article">
                                <img src="img/article2.png" class="menu-img hover1b" alt="Article" title="Article">
                	</a>
                	<div class="dropdown-content yellow-dropdown-content">
                    			<p class="dropdown-p"><a href="sellerBlogSummary.php"  class="menu-padding dropdown-a">Blog Summary</a></p>
                                <p class="dropdown-p"><a href="sellerPendingArticle.php"  class="menu-padding dropdown-a">Pending Article</a></p>
                                <p class="dropdown-p"><a href="sellerApprovedArticle.php"  class="menu-padding dropdown-a">Approved Article</a></p>
                                <p class="dropdown-p"><a href="sellerRejectedArticle.php"  class="menu-padding dropdown-a">Rejected Article</a></p>
                                <p class="dropdown-p"><a href="sellerReportedArticle.php"  class="menu-padding dropdown-a">Reported Article</a></p>
                                <p class="dropdown-p"><a href="sellerAddArticle.php"  class="menu-padding dropdown-a">Write New Article</a></p> 
                                <!-- <p class="dropdown-p"><a href="addArticle.php"  class="menu-padding dropdown-a">Write New Article</a></p>                                 -->
                	</div>
                </div>                    
                <div class="dropdown hover1">
                	<a  class="menu-margin menu-padding hover1">
                            	<img src="img/settings.png" class="menu-img hover1a" alt="Settings" title="Settings">
                                <img src="img/settings2.png" class="menu-img hover1b" alt="Settings" title="Settings">
                	</a>
                	<div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="adminChangeEmail.php"  class="menu-padding dropdown-a">Change Email</a></p>
                                <p class="dropdown-p"><a href="adminChangePassword.php"  class="menu-padding dropdown-a">Change Password</a></p>
                                <!-- <p class="dropdown-p"><a href="logout.php"  class="menu-padding dropdown-a">Logout</a></p>                                -->
                                <p class="dropdown-p"><a href="logoutAdminSeller.php"  class="menu-padding dropdown-a">Logout</a></p> 
                	</div>
                </div>                  
					
                    
                    <div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
								  <li><a href="sellerDashboard.php">Dashboard</a></li>
                                  <li><a href="profilePreview.php" class="mini-li">Profile</a></li>
                                  <li><a href="sellerPetSummary.php" class="mini-li">Pet Summary</a></li>
                                  <li><a href="allPets.php" class="mini-li">All Pets</a></li>  
                                  <li><a href="sellerBlogSummary" class="mini-li">Blog Summary</a></li>                           
                                  <li><a href="adminChangeEmail.php" class="mini-li">Change Email</a></li>
                                  <li><a href="adminChangePassword.php"  class="mini-li">Change Password</a></li>                                                                      
                                  <!-- <li  class="last-li"><a href="logout.php">Logout</a></li> -->
                                  <li  class="last-li"><a href="logoutAdminSeller.php">Logout</a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->                
                
                                       	
            </div>
        </div>

</header>
